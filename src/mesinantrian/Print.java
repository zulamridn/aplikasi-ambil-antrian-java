/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mesinantrian;

import com.github.anastaciocintra.escpos.EscPos;
import com.github.anastaciocintra.escpos.EscPosConst;
import com.github.anastaciocintra.escpos.Style;
import com.github.anastaciocintra.escpos.image.Bitonal;
import com.github.anastaciocintra.escpos.image.BitonalOrderedDither;
import com.github.anastaciocintra.escpos.image.BitonalThreshold;
import com.github.anastaciocintra.escpos.image.EscPosImage;
import com.github.anastaciocintra.escpos.image.RasterBitImageWrapper;
import com.github.anastaciocintra.output.PrinterOutputStream;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.io.IOException;
import java.time.format.TextStyle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.print.PrintService;

/**
 *
 * @author PUSKESMAS TEBAS
 */
public class Print {
   
    public void cetak(String text){
        
        String textToPrint = "Puskesmas Tebas\n"+
            "Tes Printer\n"+
             "\t\t\t\t\t\t\t"+text+"\n"+
            "Antrian Sukses\n"+
            "\n"+
            "\n\n\n\n\n\n\n";
        PrinterService printerservice = new PrinterService();
        System.out.println(printerservice.getPrinters());
        printerservice.printString("FK80 Printer", textToPrint);
        byte[] cutP = new byte[] {0x1d, 'V', 1};
        printerservice.printBytes("FK80 Printer", cutP);
    }
    
    public void printlagi() throws IOException{
        PrintService printService = PrinterOutputStream.getPrintServiceByName("FK80 Printer");
        PrinterOutputStream printerOutputStream = new PrinterOutputStream(printService);
        EscPos escpos = new EscPos(printerOutputStream);
        escpos.writeLF("Hello Wold");
        escpos.feed(5);
        escpos.cut(EscPos.CutMode.FULL);
        escpos.close();
    }
    
    public void Sample(){
        
        PrintService printService = PrinterOutputStream.getPrintServiceByName("FK80 Printer");
        EscPos escpos;
        try {
//            URL imageURL = getURL("dog.png"); 
//            BufferedImage  imageBufferedImage = ImageIO.read(imageURL);
//            RasterBitImageWrapper imageWrapper = new RasterBitImageWrapper();
            
            escpos = new EscPos(new PrinterOutputStream(printService));
            
            Style title = new Style()
                    .setBold(true)
                    .setFontSize(Style.FontSize._4, Style.FontSize._4)
                    .setJustification(EscPosConst.Justification.Center);

            Style subtitle = new Style(escpos.getStyle())
                    .setBold(true)
                    .setUnderline(Style.Underline.OneDotThick);
            Style bold = new Style(escpos.getStyle())
                    .setBold(true);
            
//            Bitonal algorithm = new BitonalThreshold(); 
//            EscPosImage escposImage = new EscPosImage(imageBufferedImage, algorithm);     
//            escpos.write(imageWrapper, escposImage);
//            escpos.feed(5);
            
            escpos.writeLF(title,"My Market")
                    .feed(3)
                    .write("Client: ")
                    .writeLF(subtitle, "John Doe")
                    .feed(3)
                    .writeLF("Cup of coffee                      $1.00")
                    .writeLF("Botle of water                     $0.50")
                    .writeLF("----------------------------------------")
                    .feed(2)
                    .writeLF(bold, 
                             "TOTAL                              $1.50")
                    .writeLF("----------------------------------------")
                    .feed(8)
                    .cut(EscPos.CutMode.FULL);
            
            
            escpos.close();
            
        } catch (IOException ex) {
            Logger.getLogger(TextStyle.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private URL getURL(String dogpng) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
