/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mesinantrian;
import com.github.anastaciocintra.escpos.EscPos;
import com.github.anastaciocintra.escpos.EscPosConst;
import com.github.anastaciocintra.escpos.Style;
import com.github.anastaciocintra.escpos.image.Bitonal;
import com.github.anastaciocintra.escpos.image.BitonalOrderedDither;
import com.github.anastaciocintra.escpos.image.BitonalThreshold;
import com.github.anastaciocintra.escpos.barcode.BarCode;
import com.github.anastaciocintra.escpos.image.CoffeeImageImpl;
import com.github.anastaciocintra.escpos.image.EscPosImage;
import com.github.anastaciocintra.escpos.image.RasterBitImageWrapper;
import com.github.anastaciocintra.output.PrinterOutputStream;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.print.PrintService;
/**
 *
 * @author PUSKESMAS TEBAS
 */
public class Image {
    @SuppressWarnings("empty-statement")
    public void GraphcsImageWrapper(String antrian, String sisa, String id, String Kat){
        System.out.println("Tes");
        // get the printer service by name passed on command line...
        //this call is slow, try to use it only once and reuse the PrintService variable.
        PrintService printService = PrinterOutputStream.getPrintServiceByName("FK80 Printer");
        EscPos escpos;
       
        try {
           
            URL url = getURL("logo.png"); 
            BufferedImage  imageBufferedImage = ImageIO.read(url);
            RasterBitImageWrapper imageWrapper = new RasterBitImageWrapper();
            
            
            escpos = new EscPos(new PrinterOutputStream(printService));
            
            BitonalOrderedDither algorithm = new BitonalOrderedDither();
            EscPosImage escposImage = new EscPosImage(new CoffeeImageImpl(imageBufferedImage), algorithm);   
            imageWrapper.setJustification(EscPosConst.Justification.Center);
            escpos.write(imageWrapper, escposImage);
            escpos.feed(3);
            
            Style title = new Style().setFontSize(Style.FontSize._1, Style.FontSize._1).setJustification(EscPosConst.Justification.Center).setBold(true);
            escpos.writeLF(title,"Puskesmas Tebas"); 
            escpos.feed(2);
            
            Style antrianstyle = new Style().setFontSize(Style.FontSize._6, Style.FontSize._6).setJustification(EscPosConst.Justification.Center).setBold(true);
            escpos.writeLF(antrianstyle ,antrian);  
            
            
            
            Style sisastyle = new Style().setFontSize(Style.FontSize._1, Style.FontSize._1).setJustification(EscPosConst.Justification.Center).setBold(true);
            escpos.writeLF(sisastyle,sisa+" / "+Kat);  
            
            BarCode barcode = new BarCode();
            
            escpos.feed(2);
            barcode.setJustification(EscPosConst.Justification.Center);
            escpos.write(barcode, id);
            escpos.feed(1);
            
            Style pesan = new Style().setFontSize(Style.FontSize._1, Style.FontSize._1).setJustification(EscPosConst.Justification.Center).setBold(true);
            escpos.writeLF(pesan,"Nomor antrian di gunakan hingga ke kasir jika anda pasien umum, simpan dan jaga struk antrian ini dengan baik."); 
            escpos.feed(1);
            
            Style web = new Style().setFontSize(Style.FontSize._1, Style.FontSize._1).setJustification(EscPosConst.Justification.Center);
            escpos.writeLF(web,"https://puskesmastebas.sambas.go.id"); 
            escpos.feed(2);

            escpos.feed(3);
            escpos.cut(EscPos.CutMode.FULL);
            
            escpos.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void Tes(){
        System.out.println("Tes");
        // get the printer service by name passed on command line...
        //this call is slow, try to use it only once and reuse the PrintService variable.
        PrintService printService = PrinterOutputStream.getPrintServiceByName("FK80 Printer");
        EscPos escpos;
       
        try {
           
            URL url = getURL("logo.png"); 
            BufferedImage  imageBufferedImage = ImageIO.read(url);
            RasterBitImageWrapper imageWrapper = new RasterBitImageWrapper();
            
            
            escpos = new EscPos(new PrinterOutputStream(printService));
            
            BitonalOrderedDither algorithm = new BitonalOrderedDither();
            EscPosImage escposImage = new EscPosImage(new CoffeeImageImpl(imageBufferedImage), algorithm);   
            imageWrapper.setJustification(EscPosConst.Justification.Center);
            escpos.write(imageWrapper, escposImage);
            escpos.feed(3);
            
            Style title = new Style().setFontSize(Style.FontSize._3, Style.FontSize._3).setJustification(EscPosConst.Justification.Center).setBold(true);
            escpos.writeLF(title,"TES PRINTER PUSKESMAS TEBAS SUKSES"); 
            escpos.feed(2);
            
            
            Style web = new Style().setFontSize(Style.FontSize._1, Style.FontSize._1).setJustification(EscPosConst.Justification.Center);
            escpos.writeLF(web,"https://puskesmastebas.sambas.go.id"); 
            escpos.feed(2);

            escpos.feed(3);
            escpos.cut(EscPos.CutMode.FULL);
            
            escpos.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void Koneksi(String koneksi){
        System.out.println("Tes");
        // get the printer service by name passed on command line...
        //this call is slow, try to use it only once and reuse the PrintService variable.
        PrintService printService = PrinterOutputStream.getPrintServiceByName("FK80 Printer");
        EscPos escpos;
       
        try {
           
            URL url = getURL("logo.png"); 
            BufferedImage  imageBufferedImage = ImageIO.read(url);
            RasterBitImageWrapper imageWrapper = new RasterBitImageWrapper();
            
            
            escpos = new EscPos(new PrinterOutputStream(printService));
            
            BitonalOrderedDither algorithm = new BitonalOrderedDither();
            EscPosImage escposImage = new EscPosImage(new CoffeeImageImpl(imageBufferedImage), algorithm);
            imageWrapper.setJustification(EscPosConst.Justification.Center);
            escpos.write(imageWrapper, escposImage);
            escpos.feed(3);
            
            Style title = new Style().setFontSize(Style.FontSize._2, Style.FontSize._2).setJustification(EscPosConst.Justification.Center).setBold(true);
            escpos.writeLF(title,koneksi); 
            escpos.feed(2);
            
            
            Style web = new Style().setFontSize(Style.FontSize._1, Style.FontSize._1).setJustification(EscPosConst.Justification.Center);
            escpos.writeLF(web,"https://puskesmastebas.sambaskab.go.id"); 
            escpos.feed(2);

            escpos.feed(3);
            escpos.cut(EscPos.CutMode.FULL);
            
            escpos.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    
   private URL  getURL(String imageName){
        String strPath =  "images/" +  imageName;
        return getClass()
                      .getClassLoader()
                      .getResource(strPath);
    }
    

}
