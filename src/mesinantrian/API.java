package mesinantrian;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author PUSKESMAS TEBAS
 */
public class API {
    
    public void getdata(int kat) throws IOException, JSONException{
//         final String POST_PARAMS = "{\n" + "\"online\": 0,\r\n" +
//        "    \"bpjs\": "+kat+",\r\n" +
//        "    \"id_pasien\": \"\"" + "\n}";
//      System.out.println(POST_PARAMS);
    URL obj = new URL("http://puskesmastebas.sambas.go.id/simpus/api/getnomor/"+"0/"+kat);
    HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
    postConnection.setRequestMethod("GET");
    postConnection.setRequestProperty("userId", "a1bcdefgh");
    postConnection.setRequestProperty("Content-Type", "application/json");
    postConnection.setDoOutput(true);
//    OutputStream os = postConnection.getOutputStream();
//    os.write(POST_PARAMS.getBytes());
//    os.flush();
//    os.close();
    int responseCode = postConnection.getResponseCode();
    System.out.println("POST Response Code :  " + responseCode);
    System.out.println("POST Response Message : " + postConnection.getResponseMessage());
    
    if (responseCode == HttpURLConnection.HTTP_OK) { //success
        BufferedReader in = new BufferedReader(new InputStreamReader(
            postConnection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in .readLine()) != null) {
            response.append(inputLine);
        } in .close();
        
        //System.out.println(response.toString());
        
        JSONObject antrianresponse = new JSONObject(response.toString());
        
        System.out.println(antrianresponse.getString("id"));
        String nomor_antrian = antrianresponse.getString("antrian");
        String sisa_antrian = antrianresponse.getString("sisa");
        String id  = antrianresponse.getString("id");
        
         if (kat == 1){
            String status = "BPJS";
            Image print = new Image();     
            print.GraphcsImageWrapper(nomor_antrian, sisa_antrian, id, status);
        }else{
            String status = "UMUM";
            Image print = new Image();     
            print.GraphcsImageWrapper(nomor_antrian, sisa_antrian, id, status);
        }
        
        
    } else {
        System.out.println("POST NOT WORKED");
    }
        
    }
    
    public void teskoneksi() throws IOException, JSONException{
      
    URL obj = new URL("http://puskesmastebas.sambas.go.id/simpus/api/koneksi");
    HttpURLConnection Connection = (HttpURLConnection) obj.openConnection();
    String readLine = null;
    Connection.setRequestMethod("GET");
    Connection.setRequestProperty("userId", "a1bcdefgh");
    int responseCode = Connection.getResponseCode();
    
    if (responseCode == HttpURLConnection.HTTP_OK) { //success
        BufferedReader in = new BufferedReader(
            new InputStreamReader(Connection.getInputStream()));
        StringBuffer response = new StringBuffer();
        while ((readLine = in .readLine()) != null) {
            response.append(readLine);
        } in .close();
        
        JSONObject koneksiresponse = new JSONObject(response.toString());
        Image print = new Image();
        print.Koneksi(koneksiresponse.getString("status"));
        
    } else {

        Image print = new Image();
        print.Koneksi("Tidak tersambung ke server");
        

    }
        
    }
    
}
